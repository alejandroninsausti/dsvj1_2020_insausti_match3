#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace match3
{
	namespace config
	{
		namespace audio
		{
			extern float masterVolume;
			extern float musicVolume;
			extern float soundVolume;

			namespace sounds
			{
				extern Sound selectOption;
				extern Sound rightSelection;
				extern Sound wrongSelection;
												
				void Update();
			}

			namespace music
			{
				extern Music menu;
				extern Music gameloop;
								
				void Update();
			}

			void InitMenu();
			void InitGameloop();
			void Init();
			void DeInitMenu();
			void DeInitGameloop();
		}		
	}
}

#endif // !AUDIO_H