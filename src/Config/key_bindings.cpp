#include "key_bindings.h"

#include "raylib.h"

namespace match3
{
	namespace config
	{
		namespace keys
		{
			KeyBindings keys;

			void Init()
			{
				SetExitKey(0);
				keys.quit = KEY_ESCAPE;
			}
		}
	}
}