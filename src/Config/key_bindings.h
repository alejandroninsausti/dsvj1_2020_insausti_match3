#ifndef KEY_BINDINGS_H
#define KEY_BINDINGS_H

namespace match3
{
	namespace config
	{
		namespace keys
		{
			struct KeyBindings
			{
				int moveLeft;
				int moveRight;
				int releaseBall;
				int quit;
			};

			void Init();

			extern KeyBindings keys;
		}
	}
}

#endif // !KEY_BINDINGS_H
