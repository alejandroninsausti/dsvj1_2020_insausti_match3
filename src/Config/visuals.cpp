#include "visuals.h"

#include "Utility/utility_menu.h"

namespace match3
{
	namespace config
	{
		namespace visuals
		{
			RESOLUTIONS res;
			int screenWidth;
			int screenHeight;

			Texture2D background;
			Texture2D whiteFilter;

			namespace menu
			{
				Font menuFont;
				Font linkFont;

				Texture2D spiral;

				void Init()
				{
					menuFont = LoadFont("res/assets/Fonts/berry.fnt");
					linkFont = LoadFont("res/assets/Fonts/arial.fnt");
					spiral = LoadTexture("res/assets/Images/spiral.png");
				}

				void DeInit()
				{
					UnloadFont(menuFont);
					UnloadFont(linkFont);
					UnloadTexture(spiral);
				}
			}

			namespace gameloop
			{
				//----------------fonts-----------------
				Font gameloopFont;
				//-----------------gems-----------------
				Texture2D shieldGem;
				Texture2D attackGem;
				Texture2D healGem;
				//--------------backgrounds--------------
				Texture2D gemsFrame;

				void Init()
				{
					gameloopFont = LoadFont("res/assets/Fonts/berry.fnt");
					shieldGem = LoadTexture("res/assets/Images/saphire.png");
					attackGem = LoadTexture("res/assets/Images/ruby.png");
					healGem = LoadTexture("res/assets/Images/emerald.png");
					gemsFrame = LoadTexture("res/assets/Images/gems_frame.png");
				}

				void DeInit()
				{
					UnloadTexture(shieldGem);
					UnloadTexture(attackGem);
					UnloadTexture(healGem);
					UnloadTexture(gemsFrame);
				}
			}

			void InitScreenSize(RESOLUTIONS newRes)
			{
				res = newRes;

				switch (res)
				{
				case RESOLUTIONS::LOW_RES:
					screenWidth = 720;
					screenHeight = 480;
					break;
				case RESOLUTIONS::MID_RES:
					screenWidth = 800;
					screenHeight = 600;
					break;
				case RESOLUTIONS::HIGH_RES:
					screenWidth = 1152;
					screenHeight = 900;
					break;
				default:
					break;
				}

				SetWindowSize(screenWidth, screenHeight);
			}

			void Init()
			{
				InitScreenSize(RESOLUTIONS::MID_RES);

				InitWindow(visuals::screenWidth, visuals::screenHeight, TextFormat("Basic Match 3 %s", utility::menu::gameVersion)); //open OpenGL window
				SetTargetFPS(60); //set max frame rate

				background = LoadTexture("res/assets/Images/background.png");
				whiteFilter = LoadTexture("res/assets/Images/white_filter.png");
				menu::Init();
			}

			void UpdateBackground()
			{
				background.width = screenWidth;
				background.height = screenHeight;
			}
		}
	}
}