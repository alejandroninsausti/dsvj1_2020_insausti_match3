#ifndef VISUALS_H
#define VISUALS_H

#include "raylib.h"

namespace match3
{
	namespace config
	{
		namespace visuals
		{
			enum class RESOLUTIONS { LOW_RES, MID_RES, HIGH_RES };

			extern RESOLUTIONS res;
			extern int screenWidth;
			extern int screenHeight;

			extern Texture2D background;
			extern Texture2D whiteFilter;

			namespace menu
			{
				extern Font menuFont;
				extern Font linkFont;

				extern Texture2D spiral;

				void Init();
				void DeInit();
			}

			namespace gameloop
			{
				//----------------fonts-----------------
				extern Font gameloopFont;
				//-----------------gems-----------------
				extern Texture2D shieldGem;
				extern Texture2D attackGem;
				extern Texture2D healGem;
				//--------------backgrounds--------------
				extern Texture2D gemsFrame;

				void Init();
				void DeInit();
			}

			void InitScreenSize(RESOLUTIONS newRes);
			void Init();
			void UpdateBackground();
		}
	}
}

#endif // !VISUALS_H
