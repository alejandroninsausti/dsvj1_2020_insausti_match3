#include "audio.h"

#include "raylib.h"

namespace match3
{
	namespace config
	{
		namespace audio
		{
			float masterVolume = 1;
			float musicVolume = 1;
			float soundVolume = 1;

			namespace sounds
			{
				Sound selectOption;
				Sound rightSelection;
				Sound wrongSelection;

				void InitMenu()
				{
					selectOption = LoadSound("res/assets/SFX/select_sound.mp3");
				}

				void InitGameloop()
				{
					rightSelection = LoadSound("res/assets/SFX/right_select_sound.mp3");
					wrongSelection = LoadSound("res/assets/SFX/wrong_select_sound.mp3");
				}

				void Update()
				{
					SetSoundVolume(selectOption, soundVolume);
				}

				void DeInitMenu()
				{
					UnloadSound(selectOption);
				}

				void DeInitGameloop()
				{
					UnloadSound(rightSelection);
					UnloadSound(wrongSelection);
				}
			}

			namespace music
			{
				Music menu;
				Music gameloop;

				void InitMenu()
				{
					if (!IsMusicPlaying(menu))
					{
						menu = LoadMusicStream("res/assets/Music/menu_ost.mp3");
						PlayMusicStream(menu);
					}
				}

				void InitGameloop()
				{
					if (!IsMusicPlaying(gameloop))
					{
						gameloop = LoadMusicStream("res/assets/Music/gameloop_ost.mp3");
						PlayMusicStream(gameloop);
					}
				}

				void Update()
				{
					SetMusicVolume(menu, musicVolume);
					SetMusicVolume(gameloop, musicVolume);
				}

				void DeInitMenu()
				{
					StopMusicStream(menu);
					UnloadMusicStream(menu);
				}

				void DeInitGameloop()
				{
					StopMusicStream(gameloop);
					UnloadMusicStream(gameloop);
				}
			}

			void InitMenu()
			{
				sounds::InitMenu();
				music::InitMenu();

				music::Update();
				sounds::Update();
			}

			void InitGameloop()
			{
				sounds::InitGameloop();
				music::InitGameloop();

				music::Update();
				sounds::Update();
			}

			void Init()
			{
				InitAudioDevice(); //init audio device
				SetMasterVolume(0.5f); //set initial volume	

				InitMenu();
			}

			void DeInitMenu()
			{
				sounds::DeInitMenu();
				music::DeInitMenu();
			}

			void DeInitGameloop()
			{
				sounds::DeInitGameloop();
				music::DeInitGameloop();
			}
		}
	}
}