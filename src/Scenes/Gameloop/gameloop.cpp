#include "gameloop.h"

#include "raylib.h"

#include "Scenes/Gameloop/Gem/gems.h"
#include "Scenes/Menu/Settings/menu_settings.h"
#include "Config/key_bindings.h"
#include "Config/visuals.h"
#include "Config/audio.h"
#include "Utility/utility_menu.h"
#include "Utility/utility_calc.h"

namespace match3
{
	namespace gameloop
	{
		bool pause;
		bool quit;

		float counter;
		short score;

		Rectangle pauseTextbox;
		Rectangle resumeTextbox;
		Rectangle optionsTextbox;
		Rectangle quitTextbox;

		Texture2D pauseFrame;

		float letterSize;
		float pauseSize;

		static void InitConfigVariables()
		{
			using namespace utility::calc;

			letterSize = NumToFloat(config::visuals::screenWidth) * 0.04f;
			pauseSize = letterSize * 3;

			pauseTextbox = utility::menu::InitTextbox(pauseSize, 6, 5, 2);
			resumeTextbox = utility::menu::InitTextbox(letterSize, 7, 5, 4.5f);
			optionsTextbox = utility::menu::InitTextbox(letterSize, 7, 5, 5.5f);
			quitTextbox = utility::menu::InitTextbox(letterSize, 17, 5, 6.5f);

			pauseFrame = config::visuals::whiteFilter;
			pauseFrame.height = NumToInt(quitTextbox.y + quitTextbox.height - pauseTextbox.y);
			pauseFrame.width = NumToInt(pauseTextbox.width * 1.4f);

			config::visuals::gameloop::Init();
			config::audio::InitGameloop();

			gems::InitConfig();
		}

		static void Init()
		{
			InitConfigVariables();

			counter = 60;
			score = 0;

			gems::Init();

			quit = false;
			pause = false;
		}

		static void DeInitConfigVariables()
		{
			config::visuals::gameloop::DeInit();
			config::audio::DeInitGameloop();
		}

		static void DeInit()
		{
			DeInitConfigVariables();

			gems::DeInit();
		}

		static void Update()
		{
			UpdateMusicStream(config::audio::music::gameloop);
			if (pause)
			{
				using namespace utility::menu;

				quit = IsKeyReleased(config::keys::keys.quit);

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					if (OptionClicked(resumeTextbox))
					{
						if (counter <= 0)
						{
							Init();
						}
						else
						{
							pause = false;
						}
					}
					if (OptionClicked(optionsTextbox))
					{
						DeInitConfigVariables();
						config::audio::InitMenu();
						config::visuals::menu::Init();
						menu::menu_settings::SettingsMenu();
						config::audio::DeInitMenu();
						config::visuals::menu::DeInit();
						InitConfigVariables();
					}
					else if (OptionClicked(quitTextbox))
					{
						quit = true;
					}
				}
			}
			else
			{
				gems::Update();
				counter -= GetFrameTime();

				pause = IsKeyReleased(config::keys::keys.quit) || counter <= 0;
			}

		}

		static void Draw()
		{
			using namespace config::visuals::menu;
			using namespace config::visuals::gameloop;
			using namespace utility::menu;
			using namespace utility::calc;

			BeginDrawing();
			//draw Background
			DrawTexture(config::visuals::background, 0, 0, WHITE);
			DrawTextOnly(gameloopFont, "PRESS and HOVER over GEMS to select them, select 3 OR MORE to score", letterSize * 0.7f, WHITE, 5, 0.5f);
			DrawTextOnly(gameloopFont, "ESC to pause", letterSize * 0.7f, WHITE, 5, 1);

			gems::Draw();
			DrawTextOnly(gameloopFont, TextFormat("Time left: %i", NumToInt(counter)), letterSize, WHITEGRAY, 3.7f, 2.9f);
			DrawTextOnly(gameloopFont, TextFormat("Score: %i", score), letterSize, WHITEGRAY, 7.8f, 2.9f);

			if (pause)
			{
				DrawTexture(pauseFrame, NumToInt(quitTextbox.x * 0.6f), NumToInt(pauseTextbox.y), GRAY);
				
				if (counter <= 0)
				{
					DrawTextOnly(gameloopFont, "Game Over", pauseSize, WHITE, 5, 2);
					DrawTextbox(gameloopFont, "Restart", resumeTextbox, letterSize, WHITE, DARKGRAY);
				}
				else
				{
					DrawTextOnly(gameloopFont, "Pause", pauseSize, WHITE, 5, 2);
					DrawTextbox(gameloopFont, "Resume", resumeTextbox, letterSize, WHITE, DARKGRAY);
				}
				
				DrawTextbox(gameloopFont, "Options", optionsTextbox, letterSize, WHITE, DARKGRAY);
				DrawTextbox(gameloopFont, "Go Back To Menu", quitTextbox, letterSize, WHITE, DARKGRAY);
			}
			ClearBackground(GRAY);
			EndDrawing();
		}

		void Gameloop()
		{
			Init();

			do
			{
				Update();
				Draw();
			} while (!quit && !WindowShouldClose());

			DeInit();
		}
	}
}