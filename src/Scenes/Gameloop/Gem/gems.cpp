#include "gems.h"

#include <iterator>
#include <vector>

#include "raylib.h"

#include "Config/visuals.h"
#include "Config/audio.h"
#include "Utility/utility_calc.h"
#include "Scenes/Gameloop/gameloop.h"

namespace match3
{
	namespace gameloop
	{
		namespace gems
		{
			enum class TYPES { INVISIBLE = -1, BROKEN = 0, ATTACK, SHIELD, HEAL };

			struct Gem
			{
				short vectorPosition;
				Vector2 texturePosition;
				Rectangle rec;
				TYPES type;
			};

			const short gridHeight = 8;
			const short gridWidth = 8;

			Gem gem[gridHeight][gridWidth];
			std::vector<Vector2> selectedGems;

			const short minMatch = 3;

			const float textureXOffset = 0.15f;
			const float textureYOffset = 0.05f;
			float textureScale;
#if _DEBUG
			float lineThick;
#endif

			static void SetPosition(short i, short j)
			{
				float xMiddle = config::visuals::screenWidth * 0.45f;
				float yMiddle = config::visuals::screenHeight * 0.6f;
				gem[i][j].rec.width = config::visuals::screenWidth / (gridWidth * 2);
				gem[i][j].rec.height = config::visuals::screenHeight / (gridHeight * 2);
				gem[i][j].rec.x = xMiddle + gem[i][j].rec.width * (j * 1.3f - gridWidth / 2);
				gem[i][j].rec.y = yMiddle + gem[i][j].rec.height * (i * 1.3f - gridHeight / 2);
			}

			static void SetTexturePosition(short i, short j)
			{
				gem[i][j].texturePosition.x = gem[i][j].rec.x + gem[i][j].rec.width * textureXOffset;
				gem[i][j].texturePosition.y = gem[i][j].rec.y - gem[i][j].rec.height * textureYOffset;
			}

			static void SetType(short i, short j)
			{
				gem[i][j].type = static_cast<TYPES>(GetRandomValue(1, 3));
			}

			void InitConfig()
			{
				for (short i = 0; i < gridHeight; i++)
				{
					for (short j = 0; j < gridWidth; j++)
					{
						SetPosition(i, j);
						SetTexturePosition(i, j);
					}
				}
			}

			void Init()
			{
				selectedGems.resize(gridHeight * gridWidth);
				selectedGems.clear();

				for (short i = 0; i < gridHeight; i++)
				{
					for (short j = 0; j < gridWidth; j++)
					{
						gem[i][j].vectorPosition = -1;
						SetPosition(i, j);
						SetTexturePosition(i, j);
						SetType(i, j);
					}
				}
			}

			static bool PositionIsValid(short i, short j)
			{
				using namespace utility::calc;

				short y = selectedGems.back().y;
				short x = selectedGems.back().x;
				return IntIsWithinRange(i, y - 1, y + 1) && IntIsWithinRange(j, x - 1, x + 1);
			}

			static bool GemIsValid(short i, short j)
			{
				if (!selectedGems.empty())
				{
					short y = selectedGems.back().y;
					short x = selectedGems.back().x;
					if (!(j == x && i == y) && gem[i][j].type == gem[y][x].type)
					{
						if (gem[i][j].vectorPosition == -1)
						{
							return PositionIsValid(i, j);
						}
						else
						{
							for (short it = selectedGems.size(); it > gem[i][j].vectorPosition; it--)
							{
								y = selectedGems.back().y;
								x = selectedGems.back().x;
								gem[y][x].vectorPosition = -1;
								selectedGems.pop_back();
							}
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return true;
				}
			}

			static void CleanGemVector()
			{
				if (selectedGems.size() >= minMatch)
				{
					for (short i = 0; i < selectedGems.size(); i++)
					{
						short y = selectedGems.at(i).y;
						short x = selectedGems.at(i).x;
						gem[y][x].type = TYPES::BROKEN;
						gem[y][x].vectorPosition = -1;
					}

					gameloop::score += selectedGems.size() * (selectedGems.size() / 5 + 1);
					PlaySound(config::audio::sounds::rightSelection);
				}
				else
				{
					for (short i = 0; i < selectedGems.size(); i++)
					{
						short y = selectedGems.at(i).y;
						short x = selectedGems.at(i).x;
						gem[y][x].vectorPosition = -1;
					}

					PlaySound(config::audio::sounds::wrongSelection);
					selectedGems.clear();
				}
			}

			static void SelectGems()
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				{
					for (short i = 0; i < gridHeight; i++)
					{
						for (short j = 0; j < gridWidth; j++)
						{
							if (CheckCollisionPointRec(GetMousePosition(), gem[i][j].rec) && GemIsValid(i, j))
							{
								Vector2 selectedGem;
								selectedGem.x = j;
								selectedGem.y = i;
								selectedGems.push_back(selectedGem);
								gem[i][j].vectorPosition = selectedGems.size();
							}
						}
					}
				}
				else if (!selectedGems.empty())
				{
					CleanGemVector();
				}
			}

			const short fallSpeed = 125;
			static bool AreGemFalling()//check if are any gems falling and make it fall
			{
				bool gemsFalling = false;

				for (short i = 0; i < gridHeight; i++)
				{
					for (short j = 0; j < gridWidth; j++)
					{
						if (gem[i][j].texturePosition.y < gem[i][j].rec.y - gem[i][j].rec.height * textureYOffset)
						{
							gemsFalling = true;
							gem[i][j].texturePosition.y += fallSpeed * GetFrameTime();
						}
					}
				}

				return gemsFalling;
			}

			static void GenerateNewGems()//check all gems from bottom to top, if it's broken, swap it with the one above
			{
				for (short j = 0; j < gridWidth; j++)
				{
					for (short i = gridHeight - 1; i >= 0; i--)
					{
						if (gem[i][j].type == TYPES::BROKEN)
						{
							if (i == 0)
							{
								gem[i][j].texturePosition.y -= gem[i][j].rec.height;
								SetType(i, j);
							}
							else
							{
								for (short k = i - 1; k >= 0; k--)
								{
									if (gem[k][j].type != TYPES::BROKEN && gem[k][j].type != TYPES::INVISIBLE)
									{
										gem[i][j].type = gem[k][j].type;
										gem[i][j].texturePosition = gem[k][j].texturePosition;

										gem[k][j].type = TYPES::BROKEN;
										
										break;
									}
								}
							}
						}
					}
				}
			}

			enum class DIRECTION { LEFT, RIGHT, UP, DOWN };
			static std::vector<Vector2> GetAdjacentComboGems(short i, short j, DIRECTION direc)
			{
				std::vector<Vector2> combo;

				if (gem[i][j].type != TYPES::BROKEN && gem[i][j].type != TYPES::INVISIBLE)
				{
					TYPES gemType = gem[i][j].type;
					Vector2 comboVec;

					switch (direc)
					{
					case DIRECTION::LEFT:
						for (short k = j - 1; k >= 0 && gem[i][k].type == gemType; k--)
						{
							comboVec.y = i;
							comboVec.x = k;
							combo.push_back(comboVec);
						}
						break;
					case DIRECTION::RIGHT:
						for (short k = j + 1; k < gridWidth && gem[i][k].type == gemType; k++)
						{
							comboVec.y = i;
							comboVec.x = k;
							combo.push_back(comboVec);
						}
						break;
					case DIRECTION::UP:
						for (short k = i - 1; k >= 0 && gem[i][k].type == gemType; k--)
						{
							comboVec.y = k;
							comboVec.x = j;
							combo.push_back(comboVec);
						}
						break;
					case DIRECTION::DOWN:
						for (short k = i + 1; k < gridHeight && gem[i][k].type == gemType; k++)
						{
							comboVec.y = k;
							comboVec.x = j;
							combo.push_back(comboVec);
						}
						break;
					default:
						break;
					}
				}

				return combo;
			}

			static bool GemComboIsPossible()
			{
				if (selectedGems.size() >= minMatch)
				{
					std::vector<Vector2> combo;
					std::vector<Vector2> tempCombo;

					for (short i = 0; i < selectedGems.size(); i++)
					{
						short y = selectedGems.at(i).y;
						short x = selectedGems.at(i).x;
						
						for (short k = 0; k < 4; k++)
						{
							if (combo.empty())
							{
								combo = GetAdjacentComboGems(y, x, static_cast<DIRECTION>(k));
							}
							else
							{
								tempCombo = GetAdjacentComboGems(y, x, static_cast<DIRECTION>(k));
								for (short l = 0; l < tempCombo.size(); l++)
								{
									combo.push_back(tempCombo.at(l));
								}
							}
						}

						if (combo.size() >= minMatch)
						{
							for (short k = 0; k < combo.size(); k++)
							{
								y = combo.at(k).y;
								x = combo.at(k).x;
								gem[y][x].type = TYPES::BROKEN;
							}

							gameloop::score += combo.size() * (combo.size() / 5 + 1);
							combo.clear();
							return true;
						}
						else
						{
							combo.clear();
							return false;
						}
					}
				}
				else
				{
					return false;
				}
			}

			void Update()
			{
				if (!AreGemFalling())
				{
					SelectGems();
					if (!selectedGems.empty() && !IsMouseButtonDown(MOUSE_LEFT_BUTTON) && !GemComboIsPossible())
					{
						selectedGems.clear();
					}
				}
				GenerateNewGems();
			}

			const float filterSizeMod = 1.0075f;
			const float frameSizeMod = 1.02f;
			static void SetFrameSizes()
			{
				using namespace config::visuals;
				using namespace config::visuals::gameloop;

				float gem0Y = gem[gridHeight - 1][0].rec.y + gem[gridHeight - 1][0].rec.height;
				float gem0X = gem[0][gridWidth - 1].rec.x + gem[0][gridWidth - 1].rec.width;

				//acomodate white filter to the grid size
				whiteFilter.height = gem0Y * filterSizeMod - gem[0][0].rec.y;
				whiteFilter.width = gem0X * filterSizeMod - gem[0][0].rec.x;

				//acomodate golden frame to the grid size
				gemsFrame.height = gem0Y * frameSizeMod - gem[0][0].rec.y;
				gemsFrame.width = gem0X * frameSizeMod - gem[0][0].rec.x;
			}

			static void UpdateDrawSizes()
			{
				using namespace config::visuals;
				using namespace config::visuals::gameloop;

				//update draw sizes
				textureScale = 0.0018f * screenWidth;
#if _DEBUG
				lineThick = 0.005f * screenWidth;
#endif

				SetFrameSizes();
			}

			static void DrawGem(short i, short j)
			{
				using namespace config::visuals::gameloop;

				UpdateDrawSizes();

#if _DEBUG				
				switch (gem[i][j].type)
				{
				case TYPES::ATTACK:
					DrawRectangleLinesEx(gem[i][j].rec, static_cast<int>(lineThick), DARKMAROON);
					break;
				case TYPES::SHIELD:
					DrawRectangleLinesEx(gem[i][j].rec, static_cast<int>(lineThick), DARKBLUE);
					break;
				case TYPES::HEAL:
					DrawRectangleLinesEx(gem[i][j].rec, static_cast<int>(lineThick), DARKGREEN);
					break;
				default:
					break;
				}
#endif // _DEBUG
				if (gem[i][j].texturePosition.y >= gem[0][j].rec.y - gem[0][j].rec.height)
				{
					switch (gem[i][j].type)
					{
					case TYPES::ATTACK:
						DrawTextureEx(attackGem, gem[i][j].texturePosition, 0, textureScale, GRAY);
						break;
					case TYPES::SHIELD:
						DrawTextureEx(shieldGem, gem[i][j].texturePosition, 0, textureScale, GRAY);
						break;
					case TYPES::HEAL:
						DrawTextureEx(healGem, gem[i][j].texturePosition, 0, textureScale, GRAY);
						break;
					default:
						break;
					}
				}
			}

			static void DrawSelectedGems(short i, short j)
			{
				using namespace config::visuals::gameloop;

				UpdateDrawSizes();

#if _DEBUG
				switch (gem[i][j].type)
				{
				case TYPES::ATTACK:
					DrawRectangleLinesEx(gem[i][j].rec, static_cast<int>(lineThick), RED);
					break;
				case TYPES::SHIELD:
					DrawRectangleLinesEx(gem[i][j].rec, static_cast<int>(lineThick), SKYBLUE);
					break;
				case TYPES::HEAL:
					DrawRectangleLinesEx(gem[i][j].rec, static_cast<int>(lineThick), GREEN);
					break;
				default:
					break;
				}
#endif // _DEBUG
				if (gem[i][j].texturePosition.y >= gem[0][j].rec.y - gem[0][j].rec.height)
				{
					switch (gem[i][j].type)
					{
					case TYPES::ATTACK:
						DrawTextureEx(attackGem, gem[i][j].texturePosition, 0, textureScale, WHITE);
						break;
					case TYPES::SHIELD:
						DrawTextureEx(shieldGem, gem[i][j].texturePosition, 0, textureScale, WHITE);
						break;
					case TYPES::HEAL:
						DrawTextureEx(healGem, gem[i][j].texturePosition, 0, textureScale, WHITE);
						break;
					default:
						break;
					}
				}
			}

			const float frameYOffset = 0.965f;
			const float frameXOffset = 0.99f;
			void Draw()
			{
				DrawTexture(config::visuals::whiteFilter, gem[0][0].rec.x * frameXOffset, gem[0][0].rec.y * frameYOffset, WHITE);

				for (short i = 0; i < gridHeight; i++)
				{
					for (short j = 0; j < gridWidth; j++)
					{
						if (gem[i][j].texturePosition.y >= gem[0][j].rec.y - gem[0][j].rec.width * 0.3f)
						{
							DrawGem(i, j);
						}
						if (gem[i][j].vectorPosition != -1)
						{
							DrawSelectedGems(i, j);
						}
					}
				}

				DrawTexture(config::visuals::gameloop::gemsFrame, gem[0][0].rec.x * frameXOffset, gem[0][0].rec.y * frameYOffset, WHITE);
			}

			void DeInit()
			{
				selectedGems.clear();
			}
		}
	}
}