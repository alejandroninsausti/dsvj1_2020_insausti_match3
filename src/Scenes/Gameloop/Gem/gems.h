#ifndef  GEMS_H
#define GEMS_H

namespace match3
{
	namespace gameloop
	{
		namespace gems
		{
			void InitConfig();
			void Init();
			void Update();
			void Draw();
			void DeInit();
		}
	}
}

#endif // ! GEMS_H