#ifndef GAMELOOP_H
#define GAMELOOP_H

namespace match3
{
	namespace gameloop
	{
		extern short score;

		void Gameloop();
	}
}

#endif // !GAMELOOP_H