#include "menu_settings.h"

#include "raylib.h"

#include "Utility/utility_menu.h"
#include "Utility/utility_keys.h"
#include "Config/visuals.h"
#include "Config/audio.h"
#include "Config/key_bindings.h"

using namespace match3::config::audio;
using namespace match3::config::visuals;
using namespace match3::config::visuals::menu;
using namespace match3::utility;

namespace match3
{
	namespace menu
	{
		namespace menu_settings
		{
			float letterSize;

			namespace settings_audio
			{
				Rectangle textBoxVolumeOnOff;

				Rectangle textBoxMusicOnOff;

				Rectangle textBoxSoundOnOff;

				Rectangle textBoxExit;;

				void Init()
				{
					letterSize = screenWidth * 0.06f;

					textBoxVolumeOnOff = utility::menu::InitTextbox(letterSize, 3, 5, 2);
					textBoxMusicOnOff = utility::menu::InitTextbox(letterSize, 3, 2.5f, 5);
					textBoxSoundOnOff = utility::menu::InitTextbox(letterSize, 3, 7.5f, 5);
					textBoxExit = utility::menu::InitTextbox(letterSize, 3.5f, 5, 9);
				}

				void DrawMuteOption(Rectangle textBox, float volume)
				{
					if (volume == 0)
					{
						utility::menu::DrawTextbox(menuFont, "OFF", textBox, letterSize, DARKPURPLE, RED);
					}
					else
					{
						utility::menu::DrawTextbox(menuFont, "ON", textBox, letterSize, DARKGREEN, GREEN);
					}
				}

				void DrawOptions()
				{
					//draw master volume option
					utility::menu::DrawTextboxTitle(menuFont, "Master Volume", 14, textBoxVolumeOnOff, letterSize, GRAY);
					DrawMuteOption(textBoxVolumeOnOff, masterVolume);

					//draw music volume option
					utility::menu::DrawTextboxTitle(menuFont, "Music Volume", 13, textBoxMusicOnOff, letterSize, GRAY);
					DrawMuteOption(textBoxMusicOnOff, musicVolume);

					//draw sound volume option
					utility::menu::DrawTextboxTitle(menuFont, "Sound Volume", 13, textBoxSoundOnOff, letterSize, GRAY);
					DrawMuteOption(textBoxSoundOnOff, soundVolume);

					//draw exit option
					utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
				}

				void Draw()
				{
					BeginDrawing();
					//draw Background
					DrawTexture(background, 0, 0, WHITE);
					//draw play, settings and exit options
					DrawOptions();
					ClearBackground(DARKGRAY);
					EndDrawing();
				}

				void AudioSettings()
				{
					Init();

					do
					{
						UpdateMusicStream(music::menu);

						//Input | Update
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (utility::menu::OptionClicked(textBoxVolumeOnOff))
							{
								PlaySound(sounds::selectOption);

								if (masterVolume == 1)
								{
									masterVolume = 0;
								}
								else
								{
									masterVolume = 1;
								}
								SetMasterVolume(0.5f * masterVolume);
							}
							else if (utility::menu::OptionClicked(textBoxSoundOnOff))
							{
								PlaySound(sounds::selectOption);

								if (soundVolume != 0)
								{
									soundVolume = 0;
								}
								else
								{
									soundVolume = 1;
								}

								sounds::Update();
							}
							else if (utility::menu::OptionClicked(textBoxMusicOnOff))
							{
								PlaySound(sounds::selectOption);

								if (musicVolume != 0)
								{
									musicVolume = 0;
								}
								else
								{
									musicVolume = 1;
								}

								music::Update();
							}
						}

						//Draw
						Draw();
					} while (!WindowShouldClose() && IsKeyUp(config::keys::keys.quit) &&
						(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

					PlaySound(sounds::selectOption);
				}
			}

			namespace settings_graphics
			{
				Rectangle textBoxActualResolution;

				Rectangle textBoxFullscreenOnOff;

				Rectangle textBoxExit;

				void Init()
				{
					letterSize = screenWidth * 0.06f;

					textBoxActualResolution = utility::menu::InitTextbox(letterSize, 8, 2.5f, 4);
					textBoxFullscreenOnOff = utility::menu::InitTextbox(letterSize, 3.5f, 7.5f, 4);
					textBoxExit = utility::menu::InitTextbox(letterSize, 3.5f, 5, 9);
				}

				void DrawActualResolution()
				{
					switch (res)
					{
					case RESOLUTIONS::LOW_RES:
						utility::menu::DrawTextbox(menuFont, "720x480", textBoxActualResolution, letterSize, ORANGE, GRAY);
						break;
					case RESOLUTIONS::MID_RES:
						utility::menu::DrawTextbox(menuFont, "800x600", textBoxActualResolution, letterSize, ORANGE, GRAY);
						break;
					case RESOLUTIONS::HIGH_RES:
						utility::menu::DrawTextbox(menuFont, "1152x900", textBoxActualResolution, letterSize, ORANGE, GRAY);
						break;
					default:
						break;
					}
				}

				void DrawFullscreenOnOffOption()
				{
					if (IsWindowFullscreen())
					{
						utility::menu::DrawTextbox(menuFont, "ON", textBoxFullscreenOnOff, letterSize, DARKGREEN, GREEN);
					}
					else
					{
						utility::menu::DrawTextbox(menuFont, "OFF", textBoxFullscreenOnOff, letterSize, DARKGREEN, GREEN);
					}
				}

				void DrawOptions()
				{
					//draw Resolution option
					utility::menu::DrawTextboxTitle(menuFont, "Resolution", 11, textBoxActualResolution, letterSize, GRAY);
					DrawActualResolution();

					//draw Fullscreen option
					utility::menu::DrawTextboxTitle(menuFont, "Fullscreen", 11, textBoxFullscreenOnOff, letterSize, GRAY);
					DrawFullscreenOnOffOption();

					//draw exit option
					utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
				}

				void Draw()
				{
					BeginDrawing();
					//draw Background
					DrawTexture(background, 0, 0, WHITE);
					//draw play, settings and exit options
					DrawOptions();
					ClearBackground(DARKGRAY);
					EndDrawing();
				}

				void GraphicsSettings()
				{
					Init();

					do
					{
						UpdateMusicStream(music::menu);

						//Input | Update
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (utility::menu::OptionClicked(textBoxActualResolution))
							{
								PlaySound(sounds::selectOption);

								switch (res)
								{
								case RESOLUTIONS::LOW_RES:
									res = RESOLUTIONS::MID_RES;
									InitScreenSize(res);
									break;
								case RESOLUTIONS::MID_RES:
									res = RESOLUTIONS::HIGH_RES;
									InitScreenSize(res);
									break;
								case RESOLUTIONS::HIGH_RES:
									res = RESOLUTIONS::LOW_RES;
									InitScreenSize(res);
									break;
								default:
									break;
								}
								Init();
								UpdateBackground();
							}
							else if (utility::menu::OptionClicked(textBoxFullscreenOnOff))
							{
								PlaySound(sounds::selectOption);

								ToggleFullscreen();
							}
						}

						//Draw
						Draw();
					} while (!WindowShouldClose() && IsKeyUp(config::keys::keys.quit) &&
						(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

					PlaySound(sounds::selectOption);
				}
			}

			Rectangle textBoxAudio;

			Rectangle textBoxGraphics;

			Rectangle textBoxExit;

			const float exitLetterModifier = 0.75f;

			void Init()
			{
				letterSize = screenWidth * 0.075f;

				textBoxAudio = utility::menu::InitTextbox(letterSize, 4.5f, 5, 3);
				textBoxGraphics = utility::menu::InitTextbox(letterSize, 8, 5, 5);
				textBoxExit = utility::menu::InitTextbox(letterSize * exitLetterModifier, 3.5f, 5, 9);
			}

			void DrawOptions()
			{
				//draw audio option
				utility::menu::DrawTextbox(menuFont, "Audio", textBoxAudio, letterSize, DARKGRAY, GRAY);

				//draw graphics option
				utility::menu::DrawTextbox(menuFont, "Graphics", textBoxGraphics, letterSize, DARKGRAY, GRAY);

				//draw exit option
				utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize * exitLetterModifier, DARKGRAY, GRAY);
			}

			void Draw()
			{
				BeginDrawing();
				//draw Background
				DrawTexture(background, 0, 0, WHITE);
				//draw all the settings and the exit options
				DrawOptions();
				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void SettingsMenu()
			{
				Init();
				do
				{
					UpdateMusicStream(music::menu);

					//Input | Update
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
					{
						if (utility::menu::OptionClicked(textBoxAudio))
						{
							PlaySound(sounds::selectOption);

							settings_audio::AudioSettings();
							Init();
						}
						else if (utility::menu::OptionClicked(textBoxGraphics))
						{
							PlaySound(sounds::selectOption);

							settings_graphics::GraphicsSettings();
							Init();
						}
					}

					//Draw
					Draw();
				} while (!WindowShouldClose() && !IsKeyReleased(config::keys::keys.quit) &&
					(!IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

				PlaySound(sounds::selectOption);
			}
		}
	}
}


