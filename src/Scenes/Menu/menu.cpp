#include "menu.h"

#include "raylib.h"

#include "Config/audio.h"
#include "Utility/utility_menu.h"
#include "Config/visuals.h"
#include "Scenes/Gameloop/gameloop.h"
#include "Scenes/Menu/Credits/menu_credits.h"
#include "Scenes/Menu/Settings/menu_settings.h"
#include "Config/key_bindings.h"

using namespace match3::config::audio;
using namespace match3::config::visuals;
using namespace match3::utility;

namespace match3
{
	namespace menu
	{
		namespace menu_main
		{
			float letterSize;
			int titleSize;


			Rectangle textBoxTitle;
			Rectangle textBoxPlay;
			Rectangle textBoxSettings;
			Rectangle textBoxCredits;
			Rectangle textBoxExit;
			
			Vector2 spiralPosition;

			void InitSpiral()
			{
				using namespace config::visuals::menu;

				spiral.width = textBoxTitle.width * 1.2f;
				spiral.height = textBoxTitle.height * 1.1f;
				spiralPosition.x = textBoxTitle.x - (spiral.width - textBoxTitle.width);
				spiralPosition.y = textBoxTitle.y - (spiral.height - textBoxTitle.height);
			}                             

			static void InitTextboxes()
			{
				letterSize = static_cast<float>(screenWidth) * 0.055f;
				titleSize = static_cast<int>(letterSize * 4);

				textBoxTitle = utility::menu::InitTextbox(titleSize, 5.5f, 5, 0);
				textBoxPlay = utility::menu::InitTextbox(letterSize, 4, 5, 5);
				textBoxSettings = utility::menu::InitTextbox(letterSize, 6.8f, 5, 6.25f);
				textBoxCredits = utility::menu::InitTextbox(letterSize, 5.8f, 5, 7.5f);
				textBoxExit = utility::menu::InitTextbox(letterSize, 3.5f, 5, 8.75f);
			}

			static void Init()
			{
				config::visuals::menu::Init();
				UpdateBackground();
				config::audio::InitMenu();

				InitTextboxes();
				InitSpiral();

				PlayMusicStream(music::menu);
			}

			static void DeInit()
			{
				config::visuals::menu::DeInit();
				config::audio::DeInitMenu();
			}

			static void DrawOptions()
			{
				using namespace match3::config::visuals::menu;


				//draw play option
				utility::menu::DrawTextbox(menuFont, "Play", textBoxPlay, letterSize, DARKGRAY, GRAY);

				//draw settings option
				utility::menu::DrawTextbox(menuFont, "Settings", textBoxSettings, letterSize, DARKGRAY, GRAY);

				//draw credits option
				utility::menu::DrawTextbox(menuFont, "Credits", textBoxCredits, letterSize, DARKGRAY, GRAY);

				//draw exit option
				utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
			}

			static void Draw()
			{
				using namespace match3::config::visuals::menu;

				BeginDrawing();
				
				//Draw Background
				DrawTexture(background, 0, 0, WHITE);

				//draw title
				DrawTextureEx(spiral, spiralPosition, 0, 1.1f, WHITE);
				utility::menu::DrawTextOnly(menuFont, "Match3", titleSize, DARKMAROON, 5, 0);
				
				//draw play, settings and exit options
				DrawOptions();

				utility::menu::DrawGameVer(menuFont, letterSize, GRAY);

				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void MainMenu()
			{
				Init();

				do
				{
					UpdateMusicStream(music::menu);

					//Input | Update
					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
					{
						if (utility::menu::OptionClicked(textBoxPlay))
						{
							PlaySound(sounds::selectOption);
							DeInit();
							gameloop::Gameloop();		
							Init();
						}
						else if (utility::menu::OptionClicked(textBoxSettings))
						{
							PlaySound(sounds::selectOption);
							menu_settings::SettingsMenu();
							InitTextboxes();
							InitSpiral();
						}
						else if (utility::menu::OptionClicked(textBoxCredits))
						{
							PlaySound(sounds::selectOption);
							menu_credits::CreditsMenu();
						}
					}

					//Draw
					Draw();
				} while (!IsKeyReleased(config::keys::keys.quit) && !WindowShouldClose() &&
					(!IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));
			}
		}
	}
}