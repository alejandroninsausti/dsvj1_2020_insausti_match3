#include "menu_credits.h"

#include "raylib.h"

#include "Config/audio.h"
#include "Utility/utility_menu.h"
#include "Config/visuals.h"
#include "Config/key_bindings.h"

using namespace match3::config::audio;
using namespace match3::config::visuals;
using namespace match3::config::visuals::menu;
using namespace match3::utility;

namespace match3
{
	namespace menu
	{
		namespace menu_credits
		{
			float letterSize;
			float letterSpacing;
			float assestSize;
			float assestSpacing;
			float linkSize;
			float linkSpacing;
			const float creditsX = 0.2f;

			Rectangle textBoxExit;

			void Init()
			{
				letterSize = screenWidth * 0.03f;
				letterSpacing = letterSize / 10;
				assestSize = letterSize * 0.85f;
				assestSpacing = assestSize / 10;
				linkSize = letterSize * 0.7f;
				linkSpacing = linkSize / 10;

				textBoxExit = utility::menu::InitTextbox(letterSize * 2, 3.5f, 5, 9.1f);

				whiteFilter.height = screenHeight * 0.8f;
				whiteFilter.width = screenWidth;
			}

			void DrawCredits()
			{
				using namespace config::visuals::menu;
				using namespace utility::menu;

				//draw library credits
				DrawCredit(menuFont, "Library used: RayLib", letterSize, GRAY, 0.5f);

				//draw graphics motor credits
				DrawCredit(menuFont, "Graphic Motor used: OpenGL", letterSize, GRAY, 1.25f);

				//draw sound editor credits
				DrawCredit(menuFont, "Sound Recorder used: Audacity", letterSize, GRAY, 2);

				//draw start of music credits
				DrawCredit(menuFont, "Music used:", letterSize, GRAY, 2.75f);

				//draw Acoustic Breeze credits
				DrawCredit(menuFont, "'Acoustic Breeze' (menu), by Bensound", assestSize, GRAY, 3.15f);

				//draw Acoustic Breeze download link
				DrawCredit(linkFont, "(downloaded in https://www.bensound.com/royalty-free-music/track/acoustic-breeze)", linkSize, GRAY, 3.55f);

				//draw Instinct credits
				DrawCredit(menuFont, "'Instinct' (gameplay), by Bensound", assestSize, GRAY, 4);

				//draw SlowMo download link
				DrawCredit(linkFont, "(downloaded in https://www.bensound.com/royalty-free-music/track/instinct)", linkSize, GRAY, 4.40f);

				//draw start of visuals credits
				DrawCredit(menuFont, "Sprites and Images used:", letterSize, GRAY, 5);

				//draw StarrySky credits
				DrawCredit(menuFont, "'Starry Night Sky' (background), by kjpargeter", assestSize, GRAY, 5.40f);

				//draw StarrySky download link
				DrawCredit(linkFont, "(downloaded in https://www.freepik.com/photos/star)", linkSize, GRAY, 5.70f);

				//draw GEMS credits
				DrawCredit(menuFont, "'Gems' (pickups), by Clint Bellanger", assestSize, GRAY, 6.15f);

				//draw GEMS download link
				DrawCredit(linkFont, "(downloaded in https://opengameart.org/content/gem-icons)", linkSize, GRAY, 6.5f);

				//draw author credits
				DrawCredit(menuFont, "Made by Alejandro Insausti", letterSize, GRAY, 8.5f);
			}

			void Draw()
			{
				BeginDrawing();

				//draw Background
				DrawTexture(background, 0, 0, WHITE);

				//draw white filter
				DrawTexture(whiteFilter, 0, screenHeight * 0.05f, DARKGRAY);
				
				//draw all credits
				DrawCredits();				

				//draw game ver
				utility::menu::DrawGameVer(menuFont, letterSize, GRAY);
				//draw exit option
				utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize * 2, DARKGRAY, GRAY);

				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void CreditsMenu()
			{
				Init();

				do
				{
					UpdateMusicStream(music::menu);

					Draw();
				} while (!WindowShouldClose() && !IsKeyReleased(config::keys::keys.quit) &&
					(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

				PlaySound(sounds::selectOption);
			}
		}
	}
}